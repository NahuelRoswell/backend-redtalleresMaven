package dao.impl;

import dao.dm.Stock;
import dao.interfaces.stockDAO;

public class StockDAOJPA extends DAOJPA<Stock> implements stockDAO {

	@Override
	public long cantidadStock() {
		return entityManager.createQuery("SELECT COUNT(stock) FROM Stock stock",
				Long.class).getSingleResult();
	}
	

}
