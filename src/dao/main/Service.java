package dao.main;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.dm.Repuesto;
import dao.dm.Stock;
import dao.dm.TuplaRepuesto;
import dao.impl.StockDAOJPA;
import dao.interfaces.stockDAO;

public class Service {
	
	private Date date = new Date();
	private Stock stock = new Stock("Stock", date);
	
	public void Inicializadora() {

		Repuesto repuesto = new Repuesto("Rueda", "Redonda de goma", "Michelin", 5999.99);
		Repuesto r2 = new Repuesto("Paragolpes", "Para no golpearse", "Golpecin", 2499.99);
		Repuesto r3 = new Repuesto("Parabrisas", "De vidrio templado", "Parabrisas Don Ivo", 1200.01);
		Repuesto r4 = new Repuesto("Bocina", "Suena como bici", "Ringring", 99.99);
		Repuesto r5 = new Repuesto("Volante", "Dirige el auto", "Volantes FE-D", 1699.5);

		TuplaRepuesto tuplaRepuesto = new TuplaRepuesto(5);
		TuplaRepuesto tr2 = new TuplaRepuesto(8);
		TuplaRepuesto tr3 = new TuplaRepuesto(3);
		TuplaRepuesto tr4 = new TuplaRepuesto(6);
		TuplaRepuesto tr5 = new TuplaRepuesto(1);

		tuplaRepuesto.agregarRepuesto(repuesto);
		tr2.agregarRepuesto(r2);
		tr3.agregarRepuesto(r3);
		tr4.agregarRepuesto(r4);
		tr5.agregarRepuesto(r5);

		stock.agregarRepuesto(tuplaRepuesto);
		stock.agregarRepuesto(tr2);
		stock.agregarRepuesto(tr3);
		stock.agregarRepuesto(tr4);
		stock.agregarRepuesto(tr5);

		stockDAO stockDAO = new StockDAOJPA();
		stockDAO.crear(stock);

		stockDAO.cerrar();
	}

	public List<String> listarRepuestos() {
		ArrayList<String> repuestos = new ArrayList<>();

		for (TuplaRepuesto tupla : stock.getRepuestos())
			repuestos.add(tupla.getRepuesto().getNombre());

		return repuestos;
	}

	public void esNumero(KeyEvent e, JTextField campo) {
		char caracter = e.getKeyChar();
		if (((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)
				&& (caracter != '-' || campo.getText().contains("-"))
				&& (caracter != '.' || campo.getText().contains("."))) {
			e.consume();
			JOptionPane.showMessageDialog(null, "Solo se admiten numeros en este campo.");
		}
	}

	public boolean verificarCampos(String entrada, String salida) {
		if ((entrada.equals("")) || (salida.equals(""))) {
			return false;
		}
		return true;
	}

	public int calcularArqueo(String repuestoSeleccionado, int entrada, int salida) {
		int total = 0;

		for (TuplaRepuesto tupla : stock.getRepuestos())
			if (tupla.getRepuesto().getNombre() == repuestoSeleccionado) {
				if (tupla.getCantidad() + entrada - salida >= 0) {
					total = tupla.getCantidad() + entrada - salida;
					tupla.setUltimaActualizacion(new Date());
					tupla.setCantidad(total);
				} else
					JOptionPane.showMessageDialog(null,
							"La cantidad vendida sobrepasa el stock. Ingrese una cantidad menor de ventas.");

			}

		actualizarDB();
		return total;
	}

	private void actualizarDB() {
		stockDAO StockDAO = new StockDAOJPA();
		System.out.println(stock.getRepuestos().size());
		StockDAO.actualizar(stock);

//		for (TuplaRepuesto tupla : stock.getRepuestos()) {
//			trdao.actualizar(tupla);
//			rdao.actualizar(tupla.getRepuesto());
//		}

//		rdao.cerrar();
//		trdao.cerrar();
		StockDAO.cerrar();
	}
}
