package dao.dm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Stock {
	
	@Id
	@GeneratedValue
	private Long id;
	private String nombre;
	private Date fecha;

	@OneToMany(mappedBy = "stock", cascade = CascadeType.ALL)
	private List<TuplaRepuesto> repuestos = new ArrayList<TuplaRepuesto>();
	
	public Stock() {}

	public Stock(String nombre, Date fecha) {
		this.nombre = nombre;
		this.fecha = fecha;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public void agregarRepuesto(TuplaRepuesto tuplarep) {
		if (!repuestos.contains(tuplarep)) {
			this.repuestos.add(tuplarep);
			tuplarep.setStock(this);
		}
	}

	public void eliminarRepuesto(TuplaRepuesto tuplaRepuesto) {
		if (repuestos.contains(tuplaRepuesto)) {
			this.repuestos.remove(tuplaRepuesto);
			tuplaRepuesto.setStock(null);
		}
	}

	public List<TuplaRepuesto> getRepuestos() {
		return repuestos;
	}

}
