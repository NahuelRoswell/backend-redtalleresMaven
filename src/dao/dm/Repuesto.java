package dao.dm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Repuesto {
	@Id
	@GeneratedValue
	private Long id;
	private String nombre, descripcion, marca;
	private double precio;
	
	@OneToOne
	private TuplaRepuesto TR;
	
	public Repuesto() {
		
	}
	
	public Repuesto(String nombre, String descripcion, String marca, double precio) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.marca = marca;
		this.precio = precio;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public TuplaRepuesto getTR() {
		return TR;
	}

	public void setTR(TuplaRepuesto tR) {
		TR = tR;
	}

}
