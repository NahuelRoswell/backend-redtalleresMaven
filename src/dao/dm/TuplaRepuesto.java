package dao.dm;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class TuplaRepuesto {
	
	@Id
	@GeneratedValue
	private Long id;
	private Date ultimaActualizacion;
	
	@OneToOne(mappedBy = "TR", cascade = CascadeType.ALL)
	private Repuesto repuesto = new Repuesto();
	private int cantidad;

	@ManyToOne
	private Stock stock;

	public TuplaRepuesto() {}

	public TuplaRepuesto(int cantidad) {
		validarCantidad(cantidad);
		
		this.ultimaActualizacion = new Date();
		this.cantidad = cantidad;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getUltimaActualizacion() {
		return ultimaActualizacion;
	}

	public void setUltimaActualizacion(Date ultimaActualizacion) {
		this.ultimaActualizacion = ultimaActualizacion;
	}

	private void validarCantidad(int cantidad) {
		if (cantidad < 0)
			throw new IllegalArgumentException("La cantidad debe ser un n�mero entero!");
	}

	public Repuesto getRepuesto() {
		return repuesto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setRepuesto(Repuesto repuesto) {
		this.repuesto = repuesto;
	}

	public void setCantidad(int cantidad) {
		validarCantidad(cantidad);

		this.cantidad = cantidad;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public void agregarStock(int cantidadIngresante) {
		cantidad += cantidadIngresante;
	}

	public void venderProducto(int cantidadAVender) {
		int total = cantidad - cantidadAVender;

		validarCantidad(total);

		cantidad = total;
	}
	
	public void agregarRepuesto(Repuesto repues) {
		if (repues.getTR() == null) {
			this.repuesto = repues;
			repues.setTR(this);
		}
	}

	public void eliminarRepuesto(Repuesto repues) {
		if (repuesto == repues) {
			this.repuesto = null;
			repues.setTR(null);
		}
	}
}
