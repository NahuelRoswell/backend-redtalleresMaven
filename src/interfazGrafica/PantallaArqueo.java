package interfazGrafica;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.main.Service;

import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class PantallaArqueo {
	
	private JFrame frmArqueoDeStock;
	private JTextField txtCantEntro;
	private JTextField txtCantSalio;
	private JTextField txtTotal;
	private Service service = new Service();
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaArqueo window = new PantallaArqueo();
					window.frmArqueoDeStock.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaArqueo() {
		service.Inicializadora();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmArqueoDeStock = new JFrame();
		frmArqueoDeStock.setResizable(false);
		frmArqueoDeStock.setTitle("Arqueo de stock");
		frmArqueoDeStock.setBounds(100, 100, 528, 443);
		frmArqueoDeStock.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmArqueoDeStock.getContentPane().setLayout(null);
		
		JLabel lblRepuesto = new JLabel("REPUESTO:");
		lblRepuesto.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblRepuesto.setBounds(10, 119, 126, 30);
		frmArqueoDeStock.getContentPane().add(lblRepuesto);
		
		JLabel lblCantidadQueEntro = new JLabel("CANTIDAD QUE ENTRO:");
		lblCantidadQueEntro.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCantidadQueEntro.setBounds(10, 180, 171, 30);
		frmArqueoDeStock.getContentPane().add(lblCantidadQueEntro);
		
		JLabel lblCantidadQueSali = new JLabel("CANTIDAD QUE SALIO:");
		lblCantidadQueSali.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCantidadQueSali.setBounds(10, 241, 171, 30);
		frmArqueoDeStock.getContentPane().add(lblCantidadQueSali);
		
		txtCantEntro = new JTextField();
		txtCantEntro.setText("0");
		txtCantEntro.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtCantEntro.setForeground(Color.BLACK);
		txtCantEntro.setBounds(191, 180, 311, 30);
		frmArqueoDeStock.getContentPane().add(txtCantEntro);
		txtCantEntro.setColumns(10);
		txtCantEntro.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
				service.esNumero(e, txtCantEntro);
			}
		});
		
		txtCantSalio = new JTextField();
		txtCantSalio.setText("0");
		txtCantSalio.setFont(new Font("Tahoma", Font.BOLD, 14));
		txtCantSalio.setForeground(Color.BLACK);
		txtCantSalio.setBounds(191, 241, 311, 30);
		frmArqueoDeStock.getContentPane().add(txtCantSalio);
		txtCantSalio.setColumns(10);
		txtCantSalio.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) 
			{
				service.esNumero(e, txtCantSalio);
			}
		});
		
		JComboBox<String> listaRepuestos = new JComboBox<String>();
		listaRepuestos.setBounds(191, 119, 311, 30);
		for (String repuesto : service.listarRepuestos()) {
			listaRepuestos.addItem(repuesto);
		}
		listaRepuestos.getSelectedItem();
		frmArqueoDeStock.getContentPane().add(listaRepuestos);
		
		JLabel lblConfirmacionArqueo = new JLabel("");
		lblConfirmacionArqueo.setHorizontalAlignment(SwingConstants.CENTER);
		lblConfirmacionArqueo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblConfirmacionArqueo.setForeground(Color.GREEN);
		lblConfirmacionArqueo.setBounds(10, 358, 193, 30);
		frmArqueoDeStock.getContentPane().add(lblConfirmacionArqueo);
		
		JLabel lblCantidadEnDeposito = new JLabel("CANTIDAD EN DEPOSITO:");
		lblCantidadEnDeposito.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCantidadEnDeposito.setBounds(10, 308, 196, 30);
		frmArqueoDeStock.getContentPane().add(lblCantidadEnDeposito);
		
		JButton btnCalcularArqueo = new JButton("CALCULAR ARQUEO");
		btnCalcularArqueo.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnCalcularArqueo.setBounds(355, 353, 147, 40);
		btnCalcularArqueo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (service.verificarCampos(txtCantEntro.getText(), txtCantSalio.getText())) {
					int entrada = Integer.parseInt(txtCantEntro.getText());
					int salida = Integer.parseInt(txtCantSalio.getText());
					
					String repuestoSeleccionado = (String) listaRepuestos.getSelectedItem();
					
					txtTotal.setText(String.valueOf(service.calcularArqueo(repuestoSeleccionado, entrada, salida)));
					
					txtCantEntro.setText("0");
					txtCantSalio.setText("0");
					
					lblConfirmacionArqueo.setText("Arqueo completado.");
				}
				else
		        {
		            JOptionPane.showMessageDialog(null, "Un campo no fue ingresado, ingrese cero (0) al campo vacio!");
		        }				
			}
		});
		frmArqueoDeStock.getContentPane().add(btnCalcularArqueo);
		
		txtTotal = new JTextField();
		txtTotal.setEditable(false);
		txtTotal.setBounds(212, 308, 147, 30);
		frmArqueoDeStock.getContentPane().add(txtTotal);
		txtTotal.setColumns(10);
		
		JLabel logoPantalla = new JLabel("");
		logoPantalla.setIcon(new ImageIcon(PantallaArqueo.class.getResource("/interfazGrafica/grualogo.png")));
		logoPantalla.setBounds(111, 11, 304, 97);
		frmArqueoDeStock.getContentPane().add(logoPantalla);

	}
}